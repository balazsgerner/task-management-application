## Task Management Application

# Description

A basic task management application where users can perform the following operations:

- Create a Task: Users can add new tasks with a title, description, and due date.
- List Tasks: Users can view a list of all their tasks.
- View Task Details: Users can click on a task to view its details, including the title, description, and due date.
- Update Task: Users can edit the details of a task (title, description, and due date).
- Delete Task: Users can delete a task.

Key concepts and components to implement in your Spring Boot WebFlux application:

-    Spring Boot WebFlux: Use Spring Boot with the WebFlux framework to handle asynchronous, non-blocking operations.
-    Reactive Programming: Implement reactive programming concepts using Flux and Mono to manage data flows and handle asynchronous operations.
-    MongoDB or Reactive Database: Store task data in a MongoDB or another reactive database to take advantage of the non-blocking capabilities of WebFlux.
-    RESTful API: Create RESTful endpoints to perform CRUD operations on tasks.
-    Validation: Implement input validation to ensure that tasks are created with valid data.
-    Error Handling: Handle errors and exceptions gracefully, returning appropriate HTTP status codes and error messages.
-    Thymeleaf or React/Vue/Angular Frontend (Optional): You can create a simple frontend using Thymeleaf templates or a frontend framework like React, Vue, or Angular to interact with your Spring Boot WebFlux backend.

Security (Optional): If you want to add an extra layer of complexity, you can implement security features like authentication and authorization.

This project will help you practice building a reactive web application using Spring Boot WebFlux and Java. You can start with the basic functionality and gradually enhance it as you become more comfortable with the technology stack.

## Building the application
### Prerequisites
- MongoDB Server instance needs to be running on port 27017
- Maven
- JDK 17

```bash
mvn clean install
```
