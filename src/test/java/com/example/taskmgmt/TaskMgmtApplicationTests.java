package com.example.taskmgmt;

import com.example.taskmgmt.model.Task;
import com.example.taskmgmt.repository.TaskRepository;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class TaskMgmtApplicationTests {
	@Autowired
	private TaskRepository taskRepository;
	@Autowired
	private WebTestClient webClient;

	@Test
	@Disabled
	void listTasks() {
		// given
		taskRepository
				.deleteAll()
				.thenMany(taskRepository.saveAll(List.of(
						new Task().setTitle("My first task").setDescription("Coffee").setDueDateTime(LocalDateTime.now().plusMinutes(10)),
						new Task().setTitle("My second task").setDescription("Make bed").setDueDateTime(LocalDateTime.now().plusMinutes(30)),
						new Task().setTitle("My third task").setDescription("Clean up").setDueDateTime(LocalDateTime.now().plusMinutes(60))
				))).subscribe();

		// when
		List<Task> result = webClient
				.get()
				.uri("/tasks")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus()
				.isOk()
				.expectBodyList(Task.class)
				// then
				.hasSize(3)
				.returnResult()
				.getResponseBody();

		assertThat(result)
				.extracting(Task::getDescription)
				.containsExactlyInAnyOrder("Coffee", "Make bed", "Clean up");
	}

}
