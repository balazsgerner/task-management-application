package com.example.taskmgmt.repository;


import com.example.taskmgmt.model.Task;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface TaskRepository extends ReactiveCrudRepository<Task, String> {
}
