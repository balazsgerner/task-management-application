package com.example.taskmgmt.controller;

import com.example.taskmgmt.model.Task;
import com.example.taskmgmt.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.thymeleaf.spring6.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;

@Controller
@RequiredArgsConstructor
@Log4j2
public class TaskController {
    private static final int DATA_STREAM_BUFFER_SIZE_ELEMENTS = 1;
    private final TaskRepository taskRepository;

    @GetMapping("/tasks")
    public String getTasks(Model model) {
        var reactiveDataDrivenMode =
                new ReactiveDataDriverContextVariable(taskRepository.findAll(), DATA_STREAM_BUFFER_SIZE_ELEMENTS);
        model.addAttribute("tasks", reactiveDataDrivenMode);
        return "index";
    }

    @GetMapping("/tasks/{id}")
    public String getTask(@PathVariable String id,  Model model) {
        model.addAttribute("tasks",
                new ReactiveDataDriverContextVariable(Flux.from(taskRepository.findById(id)), DATA_STREAM_BUFFER_SIZE_ELEMENTS));
        return "index";
    }

    @PostMapping("/tasks")
    public String createTask(@ModelAttribute Task task, Model model) {
        log.info("Attempting to save task entity: {}", task);
        taskRepository.save(task).subscribe();
        var reactiveDataDrivenMode =
                new ReactiveDataDriverContextVariable(taskRepository.findAll(), DATA_STREAM_BUFFER_SIZE_ELEMENTS);
        model.addAttribute("tasks", reactiveDataDrivenMode);
        return "index";
    }

    @GetMapping("/create-task")
    public String createTaskView(Model model) {
        model.addAttribute("task", new Task());
        return "create-task";
    }

}
